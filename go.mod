module git.snaums.de/snaums/mvoCI

go 1.16

require (
	github.com/foolin/goview v0.3.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/labstack/echo/v4 v4.2.2 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/pquerna/otp v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b
	golang.org/x/net v0.0.0-20210420210106-798c2154c571 // indirect
	golang.org/x/oauth2 v0.0.0-20210413134643-5e61552d6c78
	golang.org/x/sys v0.0.0-20210420205809-ac73e9fd8988 // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4
	gorm.io/driver/sqlserver v1.0.7
	gorm.io/gorm v1.21.8
)
