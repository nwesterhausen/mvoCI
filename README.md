# mvoCI

[![Build Status](https://mvo.stefannaumann.de/repo/state/2)](https://mvo.stefannaumann.de/public/2)

My Very Own CI-server (Continuous Integration). With it you can build Git-repositories with webhooks, i.e. whenever commits are pushed, new versions released or on the press of a button.

mvoCI aims to be simple and do as much as is necessary and nothing more. It aims to have a small memory footprint, be easy to use and understand.

**Be advised, that mvoCI uses shell access for its building routines. It is your responsibility to secure your machine from damage or data leakage by mvoCI.**

## What it does

* organize Repositories and Builds of them
* build Repositories on webhook or on click "Build Now"
* build and publish release-artifacts to Gitea automatically
* bindings to Gogs, Gitea, Gitbucket, Gitlab, Bitbucket and Github

## What it does not

* Anything more, like
  * timed builds
  * after build scripts
  * SVN, Mercurial, VCS (although there should not be a real reason for this constraint)
  * internationalization (only English)

## Requirements

### Compile-Time Requirements

Compile-Time Requirements are resolved by `go` automatically. Build with:

```
make dist
```

### Database

You need a database server, or use SQLite as database backend. You may use one of the following:
* PostgreSQL
* MySQL / MariaDB
* SQLServer
* SQLite3

## Set up

* ``./mvo --install`` for the installation dialogues
* ``./mvo`` for production mode

## Configuration and Usage

* Configuration options are in ``mvo.cfg``.

### mvo.cfg

```
LogFile              -> path to the log file
LogFileEnable        -> (bool) should a log-file be produced?
LogMode              -> write logs also to the terminal on (stderr/stdout)
app_title            -> Name your Instance
compression_method   -> compression method used for archiving build artifacts (e.g. gz)
database_provider    -> what database backend do you use?
    -> based on that more database-options, the Install dialog helps you configuring these
debug                -> (bool) more verbose output
dir_build            -> path to where the build artifacts are stored
dir_repo             -> path to where the repos are cloned to on build
host                 -> hostname of the server
http_port            -> (int) port, the server listens to
impress_*            -> information about the administrator for impress (city, mail, owner, street, zip)
impress_privacy      -> privacy statement
login_token_duration -> (int) specifies how long the login-token stays valid (in seconds)
parallel_builds      -> (int) the number of build threads and therefore the number of possible builds in parallel on the instance
```

### Lock down

Do not execute mvoCI as root. It should be used with as little permissions as possible, but enough to be useful for your usecase. Most builds execute untrusted code like automake scripts, Makefiles or the like - make sure, that this untrusted code cannot leak information from your system.

### systemd

You may use this script to start mvoCI automatically as an own user (e.g. ``mvo``). Please make sure, that that user cannot do anything you wouldn't do yourself!

```
[Unit]
Description=mvoCI
After=syslog.target
After=network.target
After=mysqld.service
#After=postgresql.service

[Service]
RestartSec=2s
Type=simple
User=mvo
Group=mvo
WorkingDirectory=/home/mvo
ExecStart=/home/mvo/mvo
Restart=always
Environment=USER=mvo HOME=/home/mvo

[Install]
WantedBy=multi-user.target
```

## Contribute

Feel free to contribute to mvoCI if there are features missing for your use case. Either send me write-up for your use case, then I'll evaluate if it is inline with my goals of mvoCI. Feel also free to send pull-requests or patches my way.

If you want to start, try adding new routes in `web/routing.go`, then fill the new route with content and a template in `views/`. You can also have a look in the REST-API (`web/api.go`) code and add new functions there, which might be useful or add new hook-code for other git-version control systems (`hooks/`).
