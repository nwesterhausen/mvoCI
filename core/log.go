package core

import (
    "os"
    "fmt"
  . "log"
    "strings"
    "runtime"
    "strconv"
)

type LogWrapper interface {
    Debug ( ...interface{} )
    Debugf ( string, ...interface{} )
    Log ( ...interface{} )
    Logf ( string, ...interface{} )
    Warn ( ...interface{} )
    Warnf ( string, ...interface{} )
    Fail ( ...interface{} )
    Failf ( string, ...interface{} )
}

const (
    DEBUG = 0
    INFO = 1
    WARN = 2
    FAIL = 3
)

var levelString []string = []string{
    "DEBUG",
    "INFO",
    "WARN",
    "FAIL",
    "INVALID",
}
var levelStringColor []string = []string {
    "\033[90m"+levelString[DEBUG]+"\033[0m",
    "\033[97m"+levelString[INFO]+"\033[0m",
    "\033[33m"+levelString[WARN]+"\033[0m",
    "\033[31;1m"+levelString[FAIL]+"\033[0m",
    "INVALID",
}
type lwrapper struct {
    ColorOutput bool
    LogLevel int
    l *Logger
    s *Logger
}

var LogFileEnable bool = false;
var LogStdoutEnable bool = true;
var LogStderrEnable bool = false;
var f *os.File
var Console lwrapper

type ServerLogger struct {
    f *os.File
}

func ServerLoggerNew ( logfile string ) ( *ServerLogger, error ) {
    var err error;
    f, err = os.OpenFile( logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644 )
    if err != nil {
        return nil, err
    }

    var s ServerLogger
    s.f = f;
    return &s, nil;
}

func (s ServerLogger) Close () {
    s.f.Close()
}

func (s ServerLogger) Write ( p []byte ) ( n int, err error ) {
    s.f.Write ( p );
    s.f.Close();
    return os.Stdout.Write ( p );
}

func (lw lwrapper) Debug ( args ...interface{} ) {
    if cfg.Debug {
        lw.outputString ( DEBUG, "", args )
    }
}

func (lw lwrapper) Debugf ( format string, args ...interface{} ) {
    if cfg.Debug {
        lw.outputString ( DEBUG, format, args )
    }
}

func (lw lwrapper) LogMode ( level int ) {
    lw.LogLevel = int(level)
}

func (lw lwrapper) Log ( args ...interface{} ) {
    lw.outputString ( INFO, "", args )
}
func (lw lwrapper) Logf ( format string, args ...interface{} ) {
    lw.outputString ( INFO, format, args )
}
func (lw lwrapper) Warn ( args ...interface{} ) {
    lw.outputString ( WARN, "", args )
}
func (lw lwrapper) Warnf ( format string, args ...interface{} ) {
    lw.outputString ( WARN, format, args )
}
func (lw lwrapper) Fail ( args ...interface{} ) {
    lw.outputString ( FAIL, "", args )
}
func (lw lwrapper) Failf ( format string, args ...interface{} ) {
    lw.outputString ( FAIL, format, args )
}


func (lw lwrapper) outputString ( level int, format string, args []interface{} ) {
    if lw.LogLevel <= level {
        str := lw.createString ( level, format, args )
        lw.l.Print ( str );
        if level > DEBUG {
            lw.s.Print ( str );
        }
    }
}

func (lw lwrapper) createString ( level int, format string, args []interface{} ) string {
    var lvl string
    var retval string

    if len ( levelString ) > level {
        if lw.ColorOutput == true {
            lvl = levelStringColor[level]
        } else {
            lvl = levelString[level]
        }
    } else {
        lvl = "INVALID"
    }

    _, file, line, ok := runtime.Caller ( 3 );   // the direct caller is a log function
    if ok != true {
        file = "invalid"
        line = -1
    }

    fs := strings.Split(file, "/")
    file = fs[len(fs)-1]
    retval = "[" + lvl + "] " + file + ":" + strconv.FormatInt(int64(line), 10) + " "
    if format == "" {
        retval += fmt.Sprintln ( args... )
    } else {
        retval += fmt.Sprintf( format, args...)
    }
    return retval;
}

func LogInit ( lf string ) (LogWrapper, error) {
    var err error;
    f, err = os.OpenFile( lf, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644 )
    if err != nil {
        return nil, err
    }

    var s *Logger
    l := New ( f, "", Ldate | Ltime )
    if LogStderrEnable {
        s = New ( os.Stderr, "", Ldate | Ltime )
    } else if LogStdoutEnable {
        s = New ( os.Stdout, "", Ldate | Ltime )
    } else {
        fnull, _ := os.OpenFile( os.DevNull, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644 )
        s = New ( fnull, "", 0 )
    }
    Console.l = l
    Console.s = s
    Console.ColorOutput = true
    return &Console, nil
}

func LogClose () {
    f.Close();
}

