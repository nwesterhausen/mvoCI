package core

import (
    "fmt"
    "time"
    "gorm.io/gorm"
    "gorm.io/gorm/logger"
    "gorm.io/driver/sqlite"
    "gorm.io/driver/postgres"
    "gorm.io/driver/mysql"
    "gorm.io/driver/sqlserver"
)

// structure representing a user
type User struct {
    ID uint `gorm:"primary_key"`    // database ID
    CreatedAt           time.Time   // time the record was created
    UpdatedAt           time.Time   // time the record was last updated
    Name                string      // the username of the user
    Passhash            string      // a hashed password
    Email               string      // email address of the user
    Superuser           bool        // is the user a superuser or not?
    Group               []*Group `gorm:"many2many:user_groups"`
    AuthProvider        string      // name of the main auth provider
    AuthProviderExtra   string      // extra information for the main auth provider
    AuthExtra           string
}

type AuthProvider struct {
    ID uint `gorm:"primary_key"`
    Name    string
    Type    string
    Extra   string
    UserId  uint
    User    User
}

// resets a user structure to a null-value
// TODO: is that used? Maybe replace with u = core.User{} ?
func (u *User) Reset () {
    u.ID = 0
    u.CreatedAt = time.Unix(0,0)
    u.UpdatedAt = time.Unix(0,0)
    u.Name = ""
    u.Passhash = ""
    u.Email = ""
    u.Superuser = false
    u.Group = []*Group{}
}

type Group struct {
    gorm.Model
    User    []*User `gorm:"many2many:user_groups"`
}

// structure for LoginTokens, used for authentification after login
type LoginToken struct {
    ID uint `gorm:"primary_key"`    // database ID
    CreatedAt time.Time             // date of creation
    ExpiresAt time.Time             // Expiration Date of the loginToken
    Name string
    UserID uint                     // Link to the user holding the Token
    User User
    Type string
    Secret string                   // a secret string
    Step string
    StepExtra string
}

type OauthToken struct {
    ID uint `gorm:"primary_key"`
    CreatedAt time.Time
    UpdatedAt time.Time
    State string
    Name string
    Api string
    Url string
    UserID uint
    ClientId string
    ClientSecret string
    ClientToken string
    RedirectUri string
}

// structure representing a Repository
type Repository struct {
    ID uint `gorm:"primary_key"`
    CreatedAt time.Time
    UpdatedAt time.Time
    Name string
    Secret string
    CloneUrl string
    BuildCount uint
    DefaultBranch string
    BuildScript string       // OBSOLETE! WILL BE REMOVED SOON
    BuildScripts []BuildScript
    WebHookEnable bool
    LocalBuildEnable bool
    KeepBuilds bool
    UserID uint
    User User
    Public bool
}

type BuildScript struct {
    ID uint `gorm:"primary_key"`
    CreatedAt time.Time
    UpdatedAt time.Time
    EventType string
    ShellScript string
    RepositoryID uint
    Repository Repository
}

// structure representing a Build of Repositories
type Build struct {
    ID uint `gorm:"primary_key"`    // database ID
    CreatedAt time.Time             // starting time of the build
    UpdatedAt time.Time             // structure last updated
    StartedAt time.Time             // the point when a build worker started the build
    FinishedAt time.Time            // time of the moment the build finished
    Status string                   // the status of the Build (started, finished, failed)
    Log string                      // build log
    CommitSha string                // stores the commit reference
    CommitAuthor string             // author of a commit
    CommitMessage string            // message of a commit
    CommitUrl string                // url to view the commit in the CVS-interface
    Event string                    // event string, why the build was built
    Branch string                   // stores the branch built
    Zip string                      // keeps the name of the zipped Build
    Repository Repository           // link to the Repository
    RepositoryID uint
    Api string
    ApiUrl string
    BuildScript BuildScript
    BuildScriptID uint
}

type WebHookLog struct {
    ID uint `gorm:"primary_key"`
    CreatedAt time.Time
    Status string
    API string
    Request string
    ResponseBody string
    ResponseStatus int
    Repository Repository           // link to the Repository
    RepositoryID uint
    Build Build                     // link to the Build
    BuildID uint
}

// parses the database configuration values and sets a configuration string
// for GORM accordingly, so it can set up the database connection.
// used by DBConnect
func connection_settings ( cfg *Config ) string {
    var x string
    switch cfg.Database.Provider {
    case "sqlite3":
        x = cfg.Database.Filename
    case "mysql":
        x = fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True",
                cfg.Database.Username,
                cfg.Database.Password,
                cfg.Database.Host,
                cfg.Database.Dbname)
    case "postgres":
        x = fmt.Sprintf("user=%s password=%s host=%s dbname=%s port=%d sslmode=%s",
                cfg.Database.Username,
                cfg.Database.Password,
                cfg.Database.Host,
                cfg.Database.Dbname,
                cfg.Database.Port,
                cfg.Database.PostgresSSL)
    case "mssql":
        x = fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s",
                cfg.Database.Username,
                cfg.Database.Password,
                cfg.Database.Host,
                cfg.Database.Port,
                cfg.Database.Dbname)
    }
    return x;
}

type gormOpenFunc func(string)gorm.Dialector;

// creates the database connection and creates the tables if they don't exist already
// depends on the following settings in the configuration ile:
//      - database_provider : sets the type of database server, i.e.
//                            postgres, sqlite3, mssql, mysql
//      - database_username : username for the database connection
//      - database_password : the password for the database connection
//      - database_host     : if mysql, postgres or mssql - host of the database
//      - database_filename : only sqlite3 - file (can be :memory)
//      - database_port     : communication port of the DB
//      - database_dbname   : the name of the database to use
func DBConnect ( cfg *Config ) (*gorm.DB, error) {
    var openFn gormOpenFunc;
    switch cfg.Database.Provider {
        case "sqlite3":
            openFn = sqlite.Open;
        case "mysql":
            openFn = mysql.Open;
        case "postgres":
            openFn = postgres.Open;
        case "mssql":
            openFn = sqlserver.Open;
    }

    gormcfg := &gorm.Config{}
    if cfg.Debug {
        gormcfg = &gorm.Config{
            Logger: logger.Default.LogMode ( logger.Info ),
        }
    }
    db,err := gorm.Open( openFn(connection_settings(cfg)), gormcfg );
    if err == nil {
        db.AutoMigrate (
            &User{},
            &AuthProvider{},
            &Group{},
            &LoginToken{},
            &Repository{},
            &Build{},
            &WebHookLog{},
            &OauthToken{},
            &BuildScript{},
        );

        return db, nil;
    } else {
        return nil, err
    }
}
