// core contains the most central elements of mvoCI, except the webserver.
// it contains the global config object and the database connection.
// could be used without the webserver to have the same backend for e.g. CLI UI
package core

import (
    "io"
    "os"
    "strings"
    "strconv"
    "reflect"
    "io/ioutil"
    "encoding/json"
)
// configuration file
const ConfigFile string = "mvo.cfg"
var BuildTime string = ""
var Compiler string = ""
var Version string = "development"
var GitHash string = ""

type ConfigDirectory struct {
    Repo string
    Build string
}
type ConfigAuthor struct {
    Email string
    Name string
    Street string
    Zip string
    City string
    Privacy string
}
type ConfigDatabase struct {
    Provider string
    Username string
    Password string
    Port int
    Host string
    Dbname string
    Filename string
    PostgresSSL string
}

type ConfigBuild struct {
    RefreshPage bool
    RefreshInterval int
}

type ConfigApi struct {
    Enable bool
    ListingNeedsAuth bool
    ListingEnable bool
}

type ConfigOauth struct {
    OauthClientId string
    OauthClientSecret string
}

// global config structure
type Config struct {
    AppTitle string
    Install bool
    Debug bool
    HttpPort int
    HttpHost string
    LogFile string
    LogServer string
    LogFileEnable bool
    LogMode string
    LoginTokenDuration int
    LoginTokenInvalidate bool
    ParallelBuilds int
    CompressionMethod string
    RepoSecretShow bool
    Directory ConfigDirectory
    Author ConfigAuthor
    Database ConfigDatabase
    WebHookLog bool
    PublicEnable bool
    Api ConfigApi
    Build ConfigBuild
    GiteaApi ConfigOauth
    Auth ConfigAuth
}

type ConfigAuth struct {
    TOTPEnable bool
    NativeEnable bool
}

func ConfigDefault () Config {
    return Config{
        AppTitle: "mvoCI",
        Install: false,
        Debug: false,
        HttpPort: 4042,
        HttpHost: "localhost",
        LoginTokenDuration: 300,
        LoginTokenInvalidate: true,
        CompressionMethod: "gz",
        ParallelBuilds: 2,
        LogFile: "log/mvoCI.log",
        LogFileEnable: true,
        LogMode: "stderr",
        LogServer: "log/server.log",
        Directory: ConfigDirectory{
            Repo: "repo/",
            Build: "builds/",
        },
        WebHookLog: false,
        PublicEnable: false,
        Api: ConfigApi {
            Enable: false,
            ListingNeedsAuth: true,
            ListingEnable: false,
        },
        Build: ConfigBuild {
            RefreshPage: true,
            RefreshInterval: 5000,      // 1000 -> 1 second
        },
        Auth: ConfigAuth {
            NativeEnable: true,
            TOTPEnable: true,
        },
        // Author -> empty strings
        // Database -> empty strings
    }
}

var cfg Config

func (cfg Config) recurseReflect ( current reflect.Value, parts []string ) string {
    var v reflect.Value = current.FieldByName ( parts[0] )
    if v.Type().Name() == "int" {
        return strconv.FormatInt( v.Int(), 10 )
    } else if v.Type().Name() == "string" {
        return v.String()
    } else if v.Type().Name() == "bool" {
        if v.Bool() {
            return "true";
        } else {
            return "false";
        }
    }
    if len (parts) > 1 {
        return cfg.recurseReflect ( v, parts[1:] )
    }
    return "Invalid Type"
}

func (cfg Config) Reflect ( q string ) string {
    var parts []string = strings.Split ( q, "." )
    current := reflect.Indirect(reflect.ValueOf(cfg))
    var r string = cfg.recurseReflect ( current, parts )
    //Console.Log ("Found value", r, "in reflecting cfg for", q)
    return r
}

func ConfigWrite ( cfg *Config, filename string ) error {
    b, err := json.MarshalIndent ( cfg, "", "  " )
    if err != nil {
        return err
    }

    err = ioutil.WriteFile ( filename, b, 0644 )
    return err
}

func GenericJSONDecode ( rd io.Reader, pl interface{} ) error {
	dec := json.NewDecoder( rd )
	for dec.More() {
		err := dec.Decode( pl )
		if err != nil {
            // early debug
            //Console.Log ( "JSON-decode error: ", err )
            return err
		}
	}
	return nil
}

// reads the configuration from file to a KeyValueStore-object
func ConfigRead ( filename string ) (Config, error) {
    cfg = ConfigDefault()
    f, err := os.Open ( filename )
    if err != nil {
        // Todo: early console
        //Console.Fail ("Cannot open config file")
        return cfg, err;
    }
    defer f.Close();

    err = GenericJSONDecode ( f, &cfg )
    if err != nil {
        // TODO: early config
        //Console.Log ( "Fatal error", err )
        //panic("")
        return cfg, err;
    }

    return cfg, nil;
}
