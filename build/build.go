// package build contains some helper functions called by the web-package to
// create a new build, rebuilding a finished (or failed) one and enqueuing them
// into the build queue (channel)
package build

import (
    "time"
    "errors"
    "git.snaums.de/snaums/mvoCI/core"
  . "git.snaums.de/snaums/mvoCI/core"

    "gorm.io/gorm"
)

// build a repository now (the latest commit, default branch)
func BuildNow ( r core.Repository, db *gorm.DB ) uint {
    bs, err := FindBuildScript ( db, r, "push" );
    if err != nil {
        Console.Warn("Build Script finding failed: ", err.Error());
        return 0;
    }
    b := core.Build { Log: "", CommitSha: "", FinishedAt: time.Unix(0,0),
                      Branch: r.DefaultBranch, BuildScript: bs,
                      Repository: r, Status : "enqueued", Event: "User Build"}
    db.Create ( &b );
    EnqueueBuild ( b );
    return b.ID
}

func FindBuildScript ( db *gorm.DB, r core.Repository, event string ) (core.BuildScript, error) {
    var bs core.BuildScript
    db.Where("repository_id=? AND event_type = ?", r.ID, event ).First ( &bs );
    if bs.ID <= 0 || len(bs.ShellScript) <= 0 {
        if event != "push" {
            db.Where("repository_id=? AND event_type = ?", r.ID, "push" ).First ( &bs );
        }
    }

    if bs.ID > 0 && len(bs.ShellScript) > 0 {
        return bs, nil;
    } else {
        return core.BuildScript{}, errors.New("No BuildScript found");
    }
}

func BuildRelease (
        db *gorm.DB,
        r core.Repository, branch string,
        sha string,
        author string,
        url string,
        message string,
        event string,
        api string,
        apiUrl string ) uint {

    bs, err := FindBuildScript ( db, r, "push" );
    if err != nil {
        Console.Warn("Build Script finding failed: ", err.Error());
        return 0;
    }

    b := core.Build { Log: "", CommitSha: sha, FinishedAt: time.Unix(0,0),
                      Branch: branch, Repository: r, Status : "enqueued",
                      CommitAuthor: author, CommitUrl: url, CommitMessage: message,
                      Event: event, Api: api, ApiUrl: apiUrl, BuildScript: bs }
    db.Create ( &b )
    EnqueueBuild ( b )
    return b.ID;
}

func BuildSpecific2 ( db *gorm.DB, r core.Repository, branch string, sha string, author string, url string, message string, event string ) uint {
    bs, err := FindBuildScript ( db, r, "push" );
    if err != nil {
        Console.Warn("Build Script finding failed: ", err.Error());
        return 0;
    }

    b := core.Build { Log: "", CommitSha: sha, FinishedAt: time.Unix(0,0),
                      Branch: branch, Repository: r, Status : "enqueued",
                      CommitAuthor: author, CommitUrl: url, CommitMessage: message,
                      Event: event, BuildScript: bs }
    db.Create ( &b )
    EnqueueBuild ( b )
    return b.ID;
}

func BuildSpecific3 ( db *gorm.DB, r core.Repository, branch string, sha string, u core.User ) uint {
    bs, err := FindBuildScript ( db, r, "push" );
    if err != nil {
        Console.Warn("Build Script finding failed: ", err.Error());
        return 0;
    }

    b := core.Build { Log: "", CommitSha: sha, FinishedAt: time.Unix(0,0),
    Branch: branch, Repository: r, Status : "enqueued", BuildScript: bs }
    db.Create ( &b )
    EnqueueBuild ( b )
    return b.ID;
}

func BuildHookedFromSecret3 ( db *gorm.DB, secret string, branch string, sha string ) uint {
    var r core.Repository
    db.Where("secret = ?", secret).First ( &r )
    if r.ID <= 0 {
        return 0;
    }
    return BuildSpecific3 ( db, r, branch, sha, core.User{} )
}

// rebuild a given build
func ReBuild ( b core.Build, db *gorm.DB ) uint {
    r := core.Repository{};
    db.Where("id = ?", b.RepositoryID).First ( &r )

    bn := core.Build { Log: "", CommitSha: b.CommitSha,
                      FinishedAt: time.Unix(0,0),  Branch: b.Branch,
                      Repository: r, Status : "enqueued", Event: "User Rebuild",
                      CommitAuthor: b.CommitAuthor, CommitMessage: b.CommitMessage,
                      CommitUrl: b.CommitUrl, BuildScript: b.BuildScript }
    db.Create ( &bn )
    EnqueueBuild ( bn );
    return bn.ID
}

// enqueue the build into the build-queue (channel)
func EnqueueBuild ( b core.Build ) error {
    core.Console.Log ("Enqueuing build")
    InChannel <- b;
    return nil;
}
