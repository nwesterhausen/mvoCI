package hook

import (
    "time"
    "errors"
    "strings"
    "golang.org/x/oauth2"
    "strconv"
    "git.snaums.de/snaums/mvoCI/core"
    "encoding/json"

    "gorm.io/gorm"
)

func GiteaPushRelease ( b *core.Build, url string, zip string, cfg *core.Config, db *gorm.DB ) error {
    // (0) Find a fitting Token in our OAuthToken Table
    var ex []string = strings.Split ( url, "/api/v1/repos" );
    var baseurl = ex[0];

    var cred core.OauthToken
    var tok oauth2.Token;
    db.Where("api = ? AND url LIKE ? AND state = ? AND user_id = ?", "gitea", baseurl, "finished", b.Repository.UserID ).First ( &cred );
    if cred.ID <= 0 {
        return errors.New("No matching OAuth-Credentials found");
    }
    err := json.Unmarshal ( []byte(cred.ClientToken), &tok );
    if err != nil {
        return err;
    }

    // (1) connect to gitea with oauth token
    client := ConnectWithToken ( cred.Url, cred.ClientId, cred.ClientSecret, []string{}, &tok, cred.RedirectUri );

    // (2) send request
    resp, err := PostFile ( client, url + "/assets", map[string]string{}, "attachment", zip )
    defer resp.Body.Close() ;
    if resp.StatusCode < 200 || resp.StatusCode > 299 {
        return errors.New ("Statuscode does not indicate success: " + strconv.Itoa(resp.StatusCode))
    }

    // (3) close everything
    return nil;
}

type GiteaPayloadCommitUser struct {
    Name string
    Email string
    Username string
}

type GiteaPayloadCommit struct {
    Id string
    Message string
    Url string
    Author GiteaPayloadCommitUser
    Committer GiteaPayloadCommitUser
    Timestamp time.Time
}

type GiteaPayloadUser struct {
    Id int
    Login string
    Full_Name string
    Email string
    Avatar_Url string
    Username string
}

type GiteaPayloadRepository struct {
    Id int
    Owner GiteaPayloadUser
    Name string
    Full_Name string
    Description string
    Private bool
    Fork bool
    Html_Url string
    Ssh_Url string
    Clone_Url string
    Website string
    Stars_Count int
    Forks_Count int
    Watcher_Count int
    Open_Issues_Count int
    Default_Branch string
    Created_At time.Time
    Updated_at time.Time
}

type GiteaPayloadRelease struct {
    Id int
    TagName string `json:"tag_name"`
    TargetCommitish string `json:"target_commitish"`
    Name string
    Body string
    Url string
    HtmlUrl string `json:"html_url"`
    Draft bool
    Prerelease bool
    Author GiteaPayloadUser
}

type GiteaPayload struct {
    Secret string
    Ref string
    Before string
    After string
    Compure_Url string
    Commits []GiteaPayloadCommit
    Repository GiteaPayloadRepository
    Pusher GiteaPayloadUser
    Sender GiteaPayloadUser
    Release GiteaPayloadRelease
}
