package hook

import (
    "time"
)

type GitBucketPayloadPusher struct {
    Name string
    Email string
    Date time.Time
}

type GitBucketPayloadUser struct {
    Login string
    Email string
    Type string
    Site_Admin bool
    Created_At time.Time
    Id int
    Url string
    Html_Url string
    Avatar_Url string
}

type GitBucketPayloadCommit struct {
    Id string
    Message string
    Timestamp time.Time
    Added []string
    Removed []string
    Modified []string
    Author GitBucketPayloadPusher
    Committer GitBucketPayloadPusher
    Url string
    Html_Url string
}

type GitBucketPayloadRepository struct {
    Name string
    Full_Name string
    Description string
    Watchers int
    Forks int
    Private bool
    Default_Branch string
    Owner GitBucketPayloadUser
    Id int
    Forks_Count int
    Watchers_Count int
    Url string
    Http_Url string
    Clone_Url string
    Html_Url string
}

type GitBucketPayload struct {
    Pusher GitBucketPayloadPusher
    Sender GitBucketPayloadUser
    Ref string
    Before string
    After string
    Commits []GitBucketPayloadCommit
    Repository GitBucketPayloadRepository
    Compare string
    Head_Commit GitBucketPayloadCommit
}



