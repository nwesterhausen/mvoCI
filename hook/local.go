package hook

type LocalPayload struct {
    Action string
    Secret string
    RepositoryID uint
    RepositoryName string
}
