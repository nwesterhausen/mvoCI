
package auth

import (
//    "fmt"
    "bytes"
    "errors"
    "strings"
    "image/png"
    "crypto/rand"
    "encoding/hex"
    "encoding/json"
    "encoding/base64"

    "git.snaums.de/snaums/mvoCI/core"
    "github.com/pquerna/otp/totp"

    "golang.org/x/crypto/bcrypt"
)

var totpProvider = Provider {
    Name : "totp",
    HumanName: "TOTP",
    Description: "Timed one time passwords (TOTP) and back-up codes",

    Verify : totpVerify,
    //SetSecret : totpSetSecret,
    Enable: totpEnable,
    EnableView: totpEnableView,
    EnableCommit: totpEnableCommit,

    Cap: ProviderCap{
        Seed: false,
        MainEnable: false,
        Instantiable: false,
    },
}

func init () {
    __init ( &totpProvider )
}

type totpExtra struct {
    Issuer string
    Email string
    Secret string
    Backup []string
}

func totpDecodeExtra ( extra string ) totpExtra {
    var t totpExtra
    rd := strings.NewReader ( extra )
    if core.GenericJSONDecode ( rd, &t ) != nil {
        return totpExtra{}
    }
    return t
}

func totpExtraMarshal ( t totpExtra ) string {
    str, _ := json.Marshal ( t )
    return string(str)
}

func totpVerify (user *core.User, stepExtra string, given string, e string, authProviderExtra string ) error {
    extra := totpDecodeExtra ( authProviderExtra )
    if extra.Secret == "" {
        return errors.New("Invalid config")
    }
    core.Console.Fail ( extra )
    if totp.Validate ( given, extra.Secret ) == true {
        core.Console.Warn ("verify checked out ", given)
        return nil
    }

    // backup codes
    for i:=0; i<len(extra.Backup); i++ {
        err := bcrypt.CompareHashAndPassword( []byte(extra.Backup[i]), []byte(given) )
        if err == nil {
            // TODO remove the used backup code and save the AuthProvider back to the database
            core.Console.Warn ("Used back-code ", given)
            return nil
        }
    }

    return errors.New("Not correct")
}

func totpBackupCodes ( num int, length int ) ( []string, error ) {
    var result []string
    for i := 0; i<num; i++ {
        var x []byte = make([]byte, length)
        _, err := rand.Read ( x )
        if err != nil {
            return []string{}, err
        }

        var xstr string = hex.EncodeToString(x)
        result = append ( result, xstr[0:15] )
    }

    return result, nil
}

func totpBackupHash ( codes []string ) []string {
    var result []string = make([]string, len(codes))
    for i := 0; i<len(codes); i ++ {
        h, _ := bcrypt.GenerateFromPassword ( []byte(codes[i]), 10 )
        result[i] = string(h)
    }
    return result
}

func totpEnable ( user core.User, prov *core.AuthProvider ) ( string, error ) {
    return "step2", nil
}

func totpEnableView ( user core.User, prov *core.AuthProvider ) ( string, error ) {
    backupCodes, err := totpBackupCodes ( 10, 20 );
    if err != nil {
        return "", err
    }
    backupCodeHashes := totpBackupHash ( backupCodes )

    var extra totpExtra = totpExtra {
        Issuer: "mvoCI",
        Email: user.Email,
        Backup: backupCodeHashes,
    }
    key, err := totp.Generate(totp.GenerateOpts{
        Issuer: extra.Issuer,
        AccountName: extra.Email,
    })
    if err != nil {
        return "", err
    }

    extra.Secret = key.Secret()

    var buf bytes.Buffer
    var b64 string
    img, err := key.Image (200, 200)
    if err != nil {
        return "", err
    }
    png.Encode ( &buf, img )
    b64 = base64.StdEncoding.EncodeToString ( buf.Bytes() )

    prov.Extra = totpExtraMarshal ( extra )

    result := `Manual:
    <dl>
      <dt>Issuer</dt><dd>` + extra.Issuer + `</dd>
      <dt>Email</dt><dd>` + extra.Email + `</dd>
      <dt>Secret</dt><dd>` + extra.Secret + `</dd>
    </dl>
    <img src="data:image/png;base64,`+b64+`"><h3>Backup-Codes:</h3>
    <pre>`
    for i := 0; i<len(backupCodes); i++ {
        result += backupCodes[i] + "\n"
    }

    result += "</pre>"
    return result, nil
}

func totpEnableCommit ( user core.User, prov *core.AuthProvider, given string ) error {
    extra := totpDecodeExtra ( prov.Extra )
    if extra.Secret == "" {
        return errors.New("Invalid config")
    }
    if totp.Validate ( given, extra.Secret ) {
        return nil
    }
    return errors.New("Not correct")
}

