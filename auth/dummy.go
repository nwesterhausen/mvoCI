package auth

import (
    "errors"
    "strconv"
    "strings"
    "math/rand"

    "git.snaums.de/snaums/mvoCI/core"
)

var dummyProvider = Provider {
    Name         : "dummy",
    HumanName    : "Dummy Authentification",
    Description  : "Check that the user can read (do not use)",

    Enable       : dummyEnable,
    EnableView   : dummyEnableView,
    EnableCommit : dummyEnableCommit,
    Verify       : dummyVerify,
    Seed         : dummySeed,
    Cap          : ProviderCap{
        Seed: true,
    },
}

func init () {
    __init ( &dummyProvider )
}

func dummyVerify (user *core.User, stepExtra string, given string, extra string, providerExtra string ) error {
    core.Console.Log ("dummyVerify", stepExtra, given, extra, providerExtra )
    g, _ := strconv.Atoi ( given )
    se, _ := strconv.Atoi ( stepExtra )
    e, _ := strconv.Atoi ( providerExtra )
    if g == se + e {
        return nil;
    } else {
        return errors.New ("Incorrect login");
    }
}

func dummySeed ( stepExtra string ) string {
    e, _  := strconv.Atoi ( stepExtra )
    return strconv.FormatInt ( (rand.Int63()%10)+(int64(e)%10)*10, 10 )
}

func dummyEnable ( _ core.User, _ *core.AuthProvider ) ( string, error ) {
    return "stage2", nil
}

func dummyEnableView ( user core.User, prov *core.AuthProvider ) ( string, error ) {
    secret := strconv.FormatInt ( rand.Int63()%10, 10 )
    challenge := strconv.FormatInt (rand.Int63()%10, 10)
    prov.Extra = secret + "," + challenge
    return "Your secret is: <b>" + secret + "</b>. Please add it to " + challenge + ".", nil
}

func dummyEnableCommit ( user core.User, prov *core.AuthProvider, given string ) error {
    o := strings.Split ( prov.Extra, "," )
    s, _ := strconv.Atoi ( o[0] )
    c, _ := strconv.Atoi ( o[1] )

    wanted := s + c
    g, _ := strconv.Atoi ( given )
    if wanted == g {
        prov.Extra = strconv.FormatInt ( int64(s), 10 );
        return nil
    }

    secret := strconv.FormatInt ( int64(s), 10 )
    challenge := strconv.FormatInt (rand.Int63()%10, 10)
    prov.Extra = secret + "," + challenge

    return errors.New("Is not good, Your secret is: <b>" + secret + "</b>. Please add it to " + challenge)
}
