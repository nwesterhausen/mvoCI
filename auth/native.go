package auth

import (
    "errors"

    "git.snaums.de/snaums/mvoCI/core"

    "golang.org/x/crypto/bcrypt"
)

var nativeProvider = Provider {
    Name        : "native",
    HumanName   : "Built-In Authentification",
    Description : "Built-In Authentification based on passwords",

    Verify      : nativeVerify,
    SetSecret   : nativeSetSecret,
}

func init () {
    __init ( &nativeProvider )
}

func checkPassword ( u *core.User, passwd string ) bool {
    err := bcrypt.CompareHashAndPassword( []byte(u.Passhash), []byte(passwd) )
    if err != nil {
        return false;
    }
    return true;
}

func createPasshash ( passwd string ) string {
    h,_ := bcrypt.GenerateFromPassword ( []byte(passwd), 10 );
    return string(h);
}

func nativeVerify (user *core.User, stepExtra string, given string, extra string, _ string ) error {
    if checkPassword ( user, given ) == true {
        return nil;
    } else {
        return errors.New ("Incorrect login");
    }
}

func nativeSetSecret ( user *core.User, pass1 string, pass2 string ) error {
    if pass1 != pass2 {
        return errors.New ("Passwords do not match");
    }

    user.Passhash = createPasshash ( pass1 );
    return nil;
}
