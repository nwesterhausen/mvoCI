// # How does auth work?
// A user has a main authentication provider, which is always used, and a list of secondary stages for auth.
// The auth modules are "loaded" in at init-time, if they are compiled in.
//
// ## Procedure on login
// ### (0) View
// First authentication view is always used for "native"-auth module (username and password).
// The problem is, that before the first user input, the user is not identifyable, therefore we do not know what main-auth to use.
// After the MainAuth checked out, the next view is identified by auth module (see user.AuthExtra.Order).
//
// ### (1) MainAuth
// First the user.AuthProvider and user.AuthProviderExtra will be used to verify the login credentials.
// It's configuration is always stored in user.AuthProviderExtra.
// 
// ### (2) Additional Steps
// After the MainAuth checked out, the user.AuthExtra field is evaluated.
// If additional steps are Enabled, the order-field identifies the names of the additional modules (comma seperated).
// They are loaded from the AuthProvider table, where their extra field is stored.
// When the order-field is exhausted, "fin" is returned as next step identifying that all steps have been traversed.
// The current step is stored in LoginToken.Step
//
// ## Secrets
// Some auth modules needs secrets, which need to be set before enabling the auth-module.
// A module can either:
//   * generate a secret on enabling, if no user interaction is required
//   * generate a secret, request the user to interact with the module, e.g. type in the first TOTP code, then commit its secret and enable itself

package auth

import (
    "fmt"
    "errors"
    "strings"
    "encoding/json"

    "git.snaums.de/snaums/mvoCI/core"

    "gorm.io/gorm"
)

type VerifyFunc func(*core.User, string, string, string, string) error
type SeedFunc func( string ) string
type EnableFunc func ( core.User, *core.AuthProvider ) ( string, error )
type SetupViewFunc func ( core.User, *core.AuthProvider ) ( string, error )
type EnableCommitFunc func ( core.User, *core.AuthProvider, string ) error
type SetSecretFunc func ( *core.User, string, string ) error

type AdminViewPostFunc func() error

// capabilities of the auth provider
type ProviderCap struct {
    Seed bool                   // the auth provider needs a seed
    SetSecretCommit bool        // the auth provider uses two-step secret verification (secret show, verify, commit secret to DB)
    ValidateRegistration bool   // allows verifying a registration
    MainEnable bool             // can be used as main authentification module
    Instantiable bool           // there can be several instances of this auth module in the system (i.e. > 1 entry in AuthProvider)
}
type Provider struct {
    Name                    string          // internal name of the auth provider, must not contain spaces, must be usable in an URL
    HumanName               string          // human readable name
    Description             string          // description of what it does or how it works

    Verify                  VerifyFunc      // verify given passowrd at login
    Seed                    SeedFunc        // seed extra information for the next step, if they are based on random
    Enable                  EnableFunc      // enable authProvider, seed for authProvider.Extra, maybe initiate the second step of verification
    EnableCommit            EnableCommitFunc    // second stage enabling - check the secret against user input, and return nil if successful

    EnableView              SetupViewFunc
    SetSecret               SetSecretFunc

    Cap                     ProviderCap
}

// list of auth providers, filled on init
var authProvider map[string]*Provider
// add an auth provider if compiled in on init-time
func __init ( pr *Provider ) {
    if authProvider == nil {
        authProvider = make ( map[string]*Provider )
    }

    authProvider[ pr.Name ] = pr
    fmt.Println ("auth: added ", pr.Name, " auth module");
}

func init () {
    fmt.Println ("auth base init")
}

// struct type for the user.AuthExtra field
// look up the extra-config for every extra step in the AuthProvider table
type AuthExtra struct {
    Enable bool     // are any extra auth steps enabled?
    Order string    // names of the auth extra-modules, comma separated
}

// decode the user.AuthExtra field
func authDecode ( user core.User ) AuthExtra {
    var a AuthExtra
    rd := strings.NewReader ( user.AuthExtra );
    if core.GenericJSONDecode ( rd, &a ) != nil {
        // this will fail all succeeding checks, i.e. the user is essentially locked out
        return AuthExtra{ Enable: true }
    }
    return a;
}

func authMarshal ( a AuthExtra ) string {
    str, _ := json.Marshal(a)
    return string(str)
}

// copy list of authProviders for a user to modify
func GetAuthProvider () []Provider {
    var result []Provider
    for _, p := range authProvider {
        result = append ( result, *p )
    }
    return result;
}

type UserProviderCap struct {
    MainAble bool           // can be used as main auth
}

// struct for reading back the authProviders for user-interaction
type UserProviderStruct struct {
    Name string             // internal name
    HumanName string        // human readable name
    Description string      // a description of what it does or how it works
    Extra string            // extra information
    InstanceName string     // name of the instance
    Cap UserProviderCap     // capabilities
    IsMain bool             // is enabled as the main auth for the user
    IsEnabled bool          // is enabled at all
}

// copy a single AuthProvider to a userProvider representation
func authToUserStructName ( name string ) UserProviderStruct {
    for _, p := range authProvider{
        if p.Name == name {
            return authToUserStruct ( *p )
        }
    }
    return UserProviderStruct{}
}

// copy struct for user-view from AuhtProvider
func authToUserStruct ( p Provider ) UserProviderStruct {
    r := UserProviderStruct {
        Name: p.Name,
        HumanName: p.HumanName,
        Description: p.Description,
        Cap : UserProviderCap{ MainAble: p.Cap.MainEnable },
        IsMain : false,
        IsEnabled : false,
    }

    if len(r.HumanName)<= 0 {
        r.HumanName = r.Name;
    }

    return r;
}

// list the authProviders for a given user
func ListAuth ( u core.User ) []UserProviderStruct {
    var prov []UserProviderStruct
    var result []UserProviderStruct
    for _, p := range authProvider{
        r := authToUserStruct ( *p );
        if r.HumanName != "" {
            prov = append ( prov, r );
        }
    }

    var mainauth UserProviderStruct = authToUserStructName ( "native" );
    for _, r := range prov {
        if u.AuthProvider == r.Name {
            mainauth = r
            mainauth.Extra = u.AuthProviderExtra
            mainauth.InstanceName = ""
            break
        }
    }
    
    mainauth.IsEnabled = true
    mainauth.IsMain = true
    result = append ( result, mainauth );

    extra := authDecode ( u );
    if extra.Enable == true {
        order := strings.Split ( extra.Order, "," );
        for _, o := range order {
            for _, r := range prov {
                if o == r.Name {
                    r.IsEnabled = true

                    result = append ( result, r );
                    break
                }
            }
        }
    }

    for _, p := range prov {
        var found bool = false
        for _, r := range result {
            if r.Name == p.Name {
                found = true;
                break
            }
        }
        if found == false {
            result = append ( result, p );
        }
    }
    return result
}

// return the human name of an auth provider given its internal name
func HumanName ( name string ) string {
    for _, p := range authProvider{
        if p.Name == name {
            return p.HumanName
        }
    }
    return ""
}

// return the name of the template of the login-view
func LoginView ( name string ) string {
    if name == "main" || name == "" {
        return "login_native"
    }
    if p, ok := authProvider[ name ]; ok {
        return "login_" + p.Name
    }

    return ""
}

func SetupView ( db *gorm.DB, user core.User, name string, challenge string ) ( string, string ) {
    var view string
    var c string = challenge
    if p, ok := authProvider[ name ]; ok {
        var prov core.AuthProvider
        db.Model(core.AuthProvider{}).Where ( "user_id = ? AND type = ?", user.ID, name ).First ( &prov );

        if prov.ID > 0 {
            if p.EnableView != nil {
                str, err := p.EnableView ( user, &prov )
                db.Save ( &prov )
                if err != nil {
                    return "", ""
                }

                c = str
            }

            view = "setup_" + p.Name
        }
    }

    return view, c
}

func AuthSetMain ( db *gorm.DB, u *core.User, name string, module string, extra string ) error {
    var prov core.AuthProvider
    err := EnsureEnabled ( db, *u, name, module, extra, &prov );
    if err != nil {
        return err
    }
    u.AuthProvider = prov.Name
    return nil
}

// enable the given module if it is not enabled already
func EnsureEnabled ( db *gorm.DB, u core.User, name string, module string, extra string, prov *core.AuthProvider ) error {
    var cnt int64
    var p core.AuthProvider
    db.Model(&core.AuthProvider{}).Where ( "user_id = ? AND type = ?", u.ID, module ).Count(&cnt).First( &p );
    if cnt == 1 {
        *prov = p
        return nil;
    } else if cnt > 1 {
        return errors.New ("There are several instances of this auth module")
    } else {
        p = core.AuthProvider { Name: name, Type: module, Extra: extra, UserId: u.ID };
        db.Create ( &p );
        *prov = p
        return nil
    }
}


// returns the string of the next authProvider given the current step from the user.AuthExtra.Order field
// fin -> finished, login successful
// fail -> failed auth, probably wrong configuration
func FollowUp ( user core.User, step string ) string {
    extra := authDecode ( user );
    if extra.Enable == false {
        return "fin";
    }

    order := strings.Split ( extra.Order, "," )
    if len(order) >= 0 {
        if step == "main" || step == "native" || step == "" {
            return order[0]
        }
        for k, o := range order {
            if o == step {
                if k == len(order)-1 {
                    return "fin"
                } else {
                    return order[k+1];
                }
            }
        }
    }
    return "fail"
}

// API for the login-handler
// verify against extra steps, outlined in user.AuthExtra
func VerifyExtra ( db *gorm.DB, user core.User, step string, stepExtra string, given string, e string, authProviderExtra string ) error {
    extra := authDecode ( user );
    if extra.Enable == false {
        return errors.New("Invalid auth operation");
    }

    order := strings.Split ( extra.Order, "," )
    var found bool = false
    for _, o := range order {
        if o == step {
            found = true
            break
        }
    }

    if found == false {
        return errors.New("Invalid auth operation");
    }

    err := AuthVerify ( &user, step, stepExtra, given, e, authProviderExtra )
    if err == nil {
        // check the username here to mitigate timing attacks
        if len(user.Name) > 1 {
            return nil
        }
        return errors.New("Invalid auth operation")
    }

    return err
}

// API for login-handler
// verifies against main auth on the user-object
func VerifyMain ( db *gorm.DB, user core.User, given string, name string, authProviderExtra string ) error {
    authname := user.AuthProvider
    authextra := user.AuthProviderExtra

    err := AuthVerify ( &user, authname, authextra, given, "", authProviderExtra );
    if err == nil {
        if len(user.Name) > 1 {
            return nil
        }
        return errors.New ("Invalid auth operation");
    }

    return err
}

func SeedStep ( name string, stepExtra string ) string {
    if p, ok := authProvider[ name ]; ok && p.Cap.Seed == true {
        return p.Seed( stepExtra )
    }
    return ""
}

func appendAuth ( user core.User, prov core.AuthProvider ) ( string, error ) {
    a := authDecode ( user )
    if a.Enable == false {
        a.Enable = true
        a.Order = prov.Name
    } else {
        o := strings.Split ( a.Order, "," )
        for _, v := range o {
            if prov.Name == v {
                return "", errors.New("Module is already enabled")
            }
        }

        if a.Order[len(a.Order)-1] != ',' {
            a.Order += ","
        }
        a.Order += prov.Name
    }
    return authMarshal ( a ), nil
}

func removeAuth ( user core.User, prov core.AuthProvider ) (string, error) {
    a := authDecode ( user )
    if a.Enable == false {
        return "", errors.New("Second stage auth was not enabled")
    } else {
        o := strings.Split ( a.Order, "," )

        newString := strings.Builder{}
        var i int = 0
        for _, v := range o {
            if prov.Name != v {
                if i > 0 {
                    newString.WriteString ( "," )
                }
                newString.WriteString ( v )
                i++
            }
        }

        if i == 0 {
            a.Enable = false
        }
        a.Order = newString.String()
    }
    return authMarshal ( a ), nil
}

func AuthEnable ( db *gorm.DB, user *core.User, name string ) ( string, error ) {
    if p, ok := authProvider[ name ]; ok {
        var prov core.AuthProvider;

        db.Where ( "user_id = ? AND type = ?", user.ID, name ).Delete(&core.LoginToken{})
        EnsureEnabled ( db, *user, name, name, "", &prov )
        str, err := p.Enable ( *user, &prov )
        db.Save ( prov )

        if err == nil && str == "" {
            str2, err2 := appendAuth ( *user, prov )
            if err2 == nil {
                db.Model( user ).Update ( "auth_extra", str2 )
            }
            return "", err2
        }

        if err != nil {
            return "", err
        }

        // first step complete, waiting for user input based on string
        return str, nil
    }

    return "", errors.New ("Auth Provider not present")
}

func AuthDisable ( db *gorm.DB, user *core.User, name string ) error {
    // we do not need to check if the auth module is built-in
    var prov core.AuthProvider
    var cnt int64
    db.Model(core.AuthProvider{}).Where ( "user_id = ? AND type = ?", user.ID, name ).Select("name,id").Count(&cnt).First ( &prov );

    if cnt < 1 {
        prov.Name = name;
        str, err := removeAuth ( *user, prov )
        if err == nil {
            db.Model(&user).Update ( "auth_extra", str )
        }
        return err
    }

    str, err := removeAuth ( *user, prov )
    if err == nil {
        db.Model(&user).Update ( "auth_extra", str )
        db.Where ( "user_id = ? AND type = ?", user.ID, name ).Delete(&core.LoginToken{})
        return nil
    }
    return err
}

func AuthEnableCommit ( db *gorm.DB, user *core.User, name string, given string ) error {
    if p, ok := authProvider[ name ]; ok {
        var prov core.AuthProvider
        var cnt int64
        db.Model(core.AuthProvider{}).Where ( "user_id = ? AND type = ?", user.ID, name ).Count(&cnt).First ( &prov );

        if cnt == 1 {
            err := p.EnableCommit ( *user, &prov, given )
            db.Save ( &prov )
            if err != nil {
                return err
            }

            str, err := appendAuth ( *user, prov )
            if err == nil {
                db.Model( user ).Update ( "auth_extra", str )
                return nil
            }
            return err
        } else {
            return errors.New("More than 1 AuthProvider with that name for that user")
        }
    }

    return errors.New("Module not found")
}


func AuthSetSecret ( user *core.User, name string, given string, extra string ) error {
    if p, ok := authProvider[ name ]; ok {
        return p.SetSecret( user, given, extra )
    }
    return errors.New ("Auth Provider not present")
}

func AuthVerify ( user *core.User, name string, stepExtra string, given string, extra string, providerExtra string ) error {
    if p, ok := authProvider[ name ]; ok {
        return p.Verify( user, stepExtra, given, extra, providerExtra )
    }
    return errors.New ("Auth Provider not present")
}
