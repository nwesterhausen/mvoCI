package web

import (
    "os"
    "net/http"
    "strconv"
    "git.snaums.de/snaums/mvoCI/core"
    "git.snaums.de/snaums/mvoCI/auth"
    "regexp"

    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

func dbConfigurationHandler ( ctx echo.Context ) ([]string,[]string) {
    provider := ctx.FormValue ( "db_type" );
    var file string;
    var host string;
    var user string;
    var passwd string;
    var port int;
    var dbname string;
    var sslrequired string;
    switch provider {
        case "sqlite3":
            file = ctx.FormValue ("sqlite3_file")
            s.cfg.Database.Provider = provider
            s.cfg.Database.Filename = file
        case "postgres":
            sslrequired = ctx.FormValue ( "postgres_ssl" )
            s.cfg.Database.PostgresSSL = sslrequired
            fallthrough
        case "mssql":
            fallthrough
        case "mysql":
            host = ctx.FormValue ( provider + "_host" )
            passwd = ctx.FormValue ( provider + "_password" )
            user = ctx.FormValue ( provider + "_user" )
            port, _ = strconv.Atoi ( ctx.FormValue ( provider + "_port" ))
            dbname = ctx.FormValue ( provider + "_database" )

            s.cfg.Database.Provider = provider
            s.cfg.Database.Host = host
            s.cfg.Database.Port = port
            s.cfg.Database.Username = user
            s.cfg.Database.Password = passwd
            s.cfg.Database.Dbname = dbname
        default:
            return []string{ "Invalid provider selected"}, []string{}
    }

    var err error
    s.db, err = core.DBConnect ( s.cfg );
    if s.db != nil {
        s.cfg.Install = false;
        core.ConfigWrite ( s.cfg, core.ConfigFile );
        s.cfg.Install = true;
        return []string {}, []string{"Database configuration saved"}
    } else {
        return []string{"Cannot connect to the database: "+err.Error()}, []string{}
    }
}

func userConfigurationHandler ( ctx echo.Context ) ([]string, []string) {
    username := ctx.FormValue ( "root_name" );
    pass1 := ctx.FormValue ( "root_password" );
    pass2 := ctx.FormValue ( "root_repeat" );
    email := ctx.FormValue ( "root_email" );

    if pass1 != pass2 {
       return []string{"Passwords were not identical"},[]string{}
    }
    if len(username) <= 0 || len(email) <= 0 || len(pass1) <= 0 {
       return []string{"Did you input anything?"},[]string{}
    }

    re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
    if re.MatchString ( email ) == false {
        return []string{"You have to input an email-address, you know?"},[]string{}
    }

    usr := core.User{Name: username, Email: email, Superuser: true}
    if err := auth.AuthSetSecret ( &usr, "native", pass1, pass2 ); err != nil {
        return []string{"Error in the auth module", err.Error()},[]string{}
    }

    s.db.Create ( &usr );
    if err := auth.AuthSetMain ( s.db, &usr, "native", "native", "" ); err == nil {
        s.db.Save ( &usr );
        return []string{},[]string{"User creation successfully"}
    } else {
        return []string{"Error in the auth module", err.Error()},[]string{}
    }
}

func renderInstallPage ( ctx echo.Context, id string, errs []string, succs []string ) error {
    file := "install" + id
    if _,err := os.Stat("views/"+file+".html"); err == nil {
        return ctx.Render ( http.StatusOK, file, echo.Map{
            "installpage": func ( i int ) string {
                return "/install/"+strconv.Itoa(i)
            },
            "errors": func () []string {
                return errs
            },
            "success": func() []string {
                return succs
            },
            "navPage": "",
        })
    } else {
        return HTTPError (http.StatusNotFound, ctx);
    }
}

func installPostHandler ( ctx echo.Context ) error {
    id := ctx.Param("id")
    next, _ := strconv.Atoi ( id );
    var errs []string;
    var succs []string;
    switch next {
        case 2:
            errs, succs = dbConfigurationHandler ( ctx )
        case 3:
            if s.db == nil {
                return ctx.String ( http.StatusInternalServerError, "No database connection found" );
            }
            errs, succs = userConfigurationHandler ( ctx )
        default :
            return ctx.Redirect ( http.StatusFound, "/install/"+id )
    }
    if len(errs) <= 0 {
        next += 1
    }
    return renderInstallPage ( ctx, strconv.Itoa(next), errs, succs )
}

func installHandler ( ctx echo.Context ) error {
    id := ctx.Param("id")
    if id == "" {
        id = "1"
    }

    return renderInstallPage ( ctx, id, []string{}, []string{} )
}

