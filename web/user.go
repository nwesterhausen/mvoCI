package web

import (
    "net/url"
    "strings"
    "strconv"
    "regexp"
    "net/http"
    "html/template"
    "git.snaums.de/snaums/mvoCI/core"
    "git.snaums.de/snaums/mvoCI/auth"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

func userHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var u []core.User;
    s.db.Order("name asc").Find ( &u );

    return ctx.Render ( http.StatusOK, "user", echo.Map{
        "navPage": "user",
        "user": user,
        "users": u,
    })
}

func userTokenAdd ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    if user.ID != uint(id) && user.Superuser != true {
        return HTTPError ( 401, ctx );
    }

    name := ctx.FormValue("user_token_name");
    var l core.LoginToken;
    if NewAPIToken ( &l, ctx, user, name ) {
        return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(user.ID)) )
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(user.ID)) )
}

func userTokenInvalidate ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    if user.ID != uint(id) && user.Superuser != true {
        return HTTPError ( 401, ctx );
    }

    tid, err := strconv.Atoi ( ctx.Param("tid") )
    if err != nil {
        return HTTPError ( 500, ctx );
    }

    s.db.Where ("id = ? AND type != 'login' AND user_id = ?", tid, user.ID ).Delete( &core.LoginToken{} );
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(user.ID)) )
}

func userRepoHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi( ctx.Param("id") )
    if err != nil {
        return HTTPError(401, ctx)
    }

    var u core.User;
    s.db.Where("id = ?", id).First ( &u )
    if u.ID > 0 && (user.ID == u.ID || user.Superuser) {
        var r []core.Repository;
        s.db.Where("user_id = ?", u.ID).Order("name ASC").Find ( &r )

        // repo_state type defined in web/repo.go
        var rx []repo_state;
        for _,v := range r {
            var bx core.Build
            s.db.Where ( "repository_id = ?", v.ID).Order("started_at DESC").Limit(1).First( &bx );
            rx = append ( rx, repo_state { R: v, State: strings.ToLower ( bx.Status ) } )
        }

        return ctx.Render ( http.StatusOK, "repo", echo.Map {
            "navPage": "repo",
            "user": user,
            "repositories": rx,
            "u": u,
        })
    }
    return ctx.Redirect ( http.StatusFound, "/user/" + strconv.Itoa(id) )
}


func userEditHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    var u core.User;
    var idstring string = ctx.Param("id")
    if idstring == "me" {
        u = user
    } else {
        id, err := strconv.Atoi(ctx.Param("id"))
        if err != nil {
            return ctx.Redirect ( http.StatusFound, "/user/me?error=Invalid ID" )
        }
        s.db.Where("id = ?", strconv.Itoa(id)).First ( &u );
        if u.ID <= 0 {
            return HTTPError ( 404, ctx );
        }
    }

    navPage := "user"
    if (u.ID == user.ID) {
        navPage = "profile";
    } else {
        if user.Superuser != true {
            HTTPError ( 401, ctx );
        }
    }

    // list of user tokens
    var token []core.LoginToken
    s.db.Where ( "user_id = ?", u.ID).Find ( &token );

    // get listing of auth modules and their config for the user
    authSetting := auth.ListAuth ( u )
    return ctx.Render ( http.StatusOK, "user_edit", echo.Map{
        "m": "edit",
        "navPage": navPage,
        "user": user,
        "u": u,
        "Auth": authSetting,
        "MainProvider": auth.HumanName ( u.AuthProvider ),
        "errors": []string{},
        "success": []string{},
        "LoginToken": token,
    })
}

func userAuthModuleEnable ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    str, err := auth.AuthEnable ( s.db, &user, module )
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me?error="+err.Error())
    }

    if str == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" enabled")
    } else {    // if str == stage2 it is transferred to the 
        // second step enabling is necessary
        return ctx.Redirect ( http.StatusFound, "/user/me/auth/setup/"+module+"?challenge="+url.QueryEscape(str) )
    }
    return ctx.Redirect ( http.StatusFound, "/user/me" )
}

func userAuthModuleSetupView ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    challenge := ctx.QueryParam ( "challenge" )
    view, challenge := auth.SetupView(s.db, user, module, challenge)
    if view == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    return ctx.Render ( http.StatusOK, view, echo.Map{
        "navPage": "profile",
        "user": user,
        "module": module,
        "challenge": template.HTML ( challenge ),
        "errors": []string{ctx.QueryParam("error")},
        "success": []string{},
    })
}

func userAuthModuleCommit ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=" + url.QueryEscape("Module not found"))
    }

    userInput := ctx.FormValue("userinput")
    if userInput == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me/"+module+"?error=Module not found!!")
    }

    err := auth.AuthEnableCommit ( s.db, &user, module, userInput )
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me/auth/setup/"+module+"?challenge="+url.QueryEscape(err.Error()) )
    }

    return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" enabled")
}

func userAuthModuleDisable ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    err := auth.AuthDisable ( s.db, &user, module )
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me?error="+err.Error())
    }
    return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" disabled")
}

func userDeleteHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }
    id, _ := strconv.Atoi(ctx.Param("id"))
    if id == int(user.ID) {
        ctx.Render ( http.StatusOK, "user_edit", echo.Map{
            "m": "edit",
            "navPage": "profile",
            "user": user,
            "u": user,
            "errors": []string{"You cannot delete yourself"},
            "success": []string{},
        })
    }
    var u core.User;
    s.db.Where("id = ?", id).First(&u);
    s.db.Delete(&u);
    return ctx.Redirect ( http.StatusFound, "/user");
}

func userAddHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }
    return ctx.Render ( http.StatusOK, "user_edit", echo.Map{
        "m": "add",
        "navPage": "user",
        "user": user,
        "u": core.User{},
        "errors": []string{},
        "success": []string{},
    })
}

func userPostChecker ( ctx echo.Context, edit bool ) ([]string, []string, uint) {
    var id int;
    var u core.User;
    if edit == true {
        id, _ = strconv.Atoi ( ctx.Param("id") );
        s.db.Where("ID = ?", id).First (&u)
    }
    var name string;
    var pass1 string;
    var pass2 string;
    var email string;
    var superuser string;

    name = ctx.FormValue ( "user_name" );
    email = ctx.FormValue ( "user_email" );
    pass1 = ctx.FormValue ( "user_password" );
    pass2 = ctx.FormValue ( "user_password2" );
    superuser = ctx.FormValue ("user_superuser" );

    if pass1 != pass2 {
       return []string{"Passwords were not identical"},[]string{},0
    }
    if len(email) <= 0 || ((len(name) <= 0|| len(pass1) <= 0) && edit == false)  {
       return []string{"Did you input anything?"},[]string{},0
    }
    var cnt int64 = 0
    s.db.Model(&core.User{}).Where("name = ?", name).Count(&cnt)
    if cnt > 0 {
       return []string{"Username already taken"},[]string{},0
    }
    re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
    if re.MatchString ( email ) == false {
        return []string{"You have to input an email-address, you know?"},[]string{},0
    }

    if edit == false {
        u = core.User{};
        u.Name = name;
    }
    u.Email = email;
    if superuser == "on" {
        u.Superuser = true;
    } else {
        u.Superuser = false;
    }
    if len(pass1) > 0 {
        if ( auth.AuthSetSecret ( &u, "native", pass1, pass2 ) != nil ) {
            return []string{"Passwords were not identical"},[]string{},0
        }
    }

    if edit == false {
        s.db.Create( &u );
    } else {
        s.db.Model(&core.User{}).Where("id = ?", u.ID).Save ( &u );
    }
    return []string{}, []string{"ok"},u.ID
}

func userEditPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }

    id, _ := strconv.Atoi( ctx.Param("id") );
    err, _, uid := userPostChecker ( ctx, true );
    if len(err) >= 0 {
        var u core.User;
        s.db.Where("id = ?", id).First(&u);
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.FormatUint(uint64(uid), 10, ) )
}

func userAddPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }

    err, _, uid := userPostChecker ( ctx, false );
    if len(err) > 0 {
        return ctx.Render ( http.StatusOK, "user_edit", echo.Map{
            "m": "add",
            "navPage": "user",
            "user": user,
            "u": core.User{},
            "errors": err,
            "success": []string{},
        })
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.FormatUint(uint64(uid), 10) )
}
