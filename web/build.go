package web

import (
    "os"
    "time"
    //"fmt"
    "strconv"
    //"regexp"
    "net/http"
    "git.snaums.de/snaums/mvoCI/core"
    "git.snaums.de/snaums/mvoCI/build"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

func zipDownloadHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    file := ctx.Param ( "file" );
    var b core.Build;
    s.db.Where("zip = ?", file).First( &b );
    s.db.Preload("Repository").Model( &b );
    if user.Superuser == false && user.ID != b.Repository.UserID {
        return HTTPError ( 404, ctx );
    }

    return ctx.File ( s.cfg.Directory.Build + file );
}

func buildViewHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var b core.Build;
    var r core.Repository;
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    s.db.Where("id = ?", id).First ( &b )
    s.db.Select ("name, id, user_id").Where("id = ?", b.RepositoryID).First( &r )

    if user.Superuser == false && r.UserID != user.ID {
        return HTTPError ( 401, ctx )
    }

    unfinished := false
    duration := ""
    if b.Status == "finished" || b.Status == "failed" {
        d := b.FinishedAt.Sub (b.StartedAt)
        duration = d.Truncate(time.Millisecond).String ()
    } else {
        unfinished = true
    }

    return ctx.Render ( http.StatusOK, "build", echo.Map{
        "navPage": "repo",
        "user": user,
        "repo": r,
        "build": b,
        "duration": duration,
        "unfinished": unfinished,
    })
}

func buildLogHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var b core.Build;
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    var r core.Repository
    s.db.Select("log, repository_id").Where("id = ?", id).First ( &b )
    s.db.Select("user_id").Where("id = ?", b.RepositoryID).First ( &r )
    if user.Superuser || user.ID == r.UserID {
        if len(b.Log) != 0 {
            return ctx.String ( http.StatusOK, b.Log )
        }
    }
    return HTTPError ( 404, ctx );
}

func buildDeleteHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    var b core.Build
    var r core.Repository
    s.db.Select("id, repository_id, zip").Where("id = ?", id).First ( &b )
    s.db.Select("user_id").Where("id = ?", b.RepositoryID).First ( &r )
    var rID uint = r.ID
    if user.Superuser || user.ID == r.UserID {
        rID = buildDelete ( uint(id) )
    }
    return ctx.Redirect ( http.StatusFound, "/repo/view/" + strconv.Itoa(int(rID)) )
}

func buildDelete ( id uint ) uint {
    var rID uint = 0
    var b core.Build;

    s.db.Select("id, repository_id, zip").Where("id = ?", id).First ( &b )
    rID = b.RepositoryID
    os.Remove ( s.cfg.Directory.Build + b.Zip )
    s.db.Delete( &b )
    return rID
}

func buildRebuildHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    var b core.Build;
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    var bID uint = 0
    var r core.Repository = core.Repository{}
    s.db.Where("id = ?", id).First ( &b )
    s.db.Where("id = ?", b.RepositoryID).First ( &r )
    if r.ID > 0 && (user.Superuser || user.ID == r.UserID) {
        bID = build.ReBuild ( b, s.db );
        return ctx.Redirect ( http.StatusFound, "/build/" + strconv.Itoa(int(bID)) )
    } else {
        return HTTPError ( 401, ctx )
    }
}
