package web

import (
    "time"
    "fmt"
    "io"
    "errors"
    "net/http"
    "crypto/rand"
    "encoding/hex"

    "git.snaums.de/snaums/mvoCI/core"
    "github.com/labstack/echo/v4"
//    "golang.org/x/crypto/bcrypt"
)

const sessionCookie string = "mvoCI_session"

func init () {
    // Assert that a cryptographically secure PRNG is available.
	// Panic otherwise.
	buf := make([]byte, 1)

	_, err := io.ReadFull(rand.Reader, buf)
	if err != nil {
		panic(fmt.Sprintf("crypto/rand is unavailable: Read() failed with %#v", err))
	}
}

func RemoveCookie ( ctx echo.Context, name string ) {
    r := CookieExists (  ctx, name )
    if r == true {
        WriteCookie ( ctx, name, "", time.Unix(0, 0));
    }
}

func WriteCookie ( ctx echo.Context, name string, value string, expires time.Time ) {
    r := CookieExists (  ctx, name )
    var cookie *http.Cookie
    if r == true {
        cookie = CollectCookie ( ctx, name );
    } else {
        cookie = new (http.Cookie)
        cookie.Name = name
    }
    cookie.Value = value
    cookie.Expires = expires;
    ctx.SetCookie( cookie )
}

func CookieExists ( ctx echo.Context, name string ) bool {
    cookie, err := ctx.Cookie( name )
	if err != nil || cookie == nil{
		return false
	}
    return true
}

func CollectCookie ( ctx echo.Context, name string ) *http.Cookie {
    cookie, err := ctx.Cookie( name )
	if err != nil || cookie == nil{
		return nil
	}

    return cookie;
}

func ReadCookie ( ctx echo.Context, name string ) (string, error) {
    cookie, err := ctx.Cookie( name )
	if err != nil || cookie == nil{
		return "", errors.New("No Cookie found")
	}

    return cookie.Value, nil;
}

func UserFromSession ( u *core.User, ctx echo.Context ) bool {
    return userFromSession ( u, false, false, ctx );
}

func UserFromIncompleteSession ( u *core.User, ctx echo.Context ) bool {
    return userFromSession ( u, false, true, ctx );
}

func ApiTokenFromSession ( u *core.User, ctx echo.Context ) bool {
    return userFromSession ( u, true, false, ctx );
}

func loginStepFromToken ( ctx echo.Context ) (string,string) {
    hash, err := ReadCookie ( ctx, sessionCookie );
    if err != nil {
        return "",""
    }

    var count int64;
    l := core.LoginToken{};
    s.db.Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ?", hash, time.Now() ).Count(&count).First( &l );

    if hash != "" && count == 1 {
        return l.Step, l.StepExtra;
    }

    return "","";
}


func userFromSession ( u *core.User, api bool, incomplete bool, ctx echo.Context ) bool {
    hash, err := ReadCookie ( ctx, sessionCookie );
    if err != nil {
        *u = core.User{}
        return false
    }

    if hash == "" {
        u.Reset ();
        return false;
    }
    var count int64;
    l := core.LoginToken{};
    if api {
        // users _and_ API-tokens can call the API
        s.db.Preload("User").Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ?", hash, time.Now() ).Count(&count).First( &l );
    } else {
        if incomplete == true {
            s.db.Preload("User").Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ? AND type = 'login'", hash, time.Now() ).Count(&count).First( &l );
        } else {
            s.db.Preload("User").Model(&core.LoginToken{}).Where("secret = ? AND expires_at > ? AND type = 'login' AND step = ?", hash, time.Now(), "fin" ).Count(&count).First( &l );
        }
    }
    if count == 1 {
        *u = l.User
        return true;
    }
    u.Reset();
    return false;
}

func authSecretGenerator () string {
    b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "AN ERROR OCCURED, PLEASE REPORT!"
    }
    hs := hex.EncodeToString ( b )
    result := hs[0:4] + "-" + hs[4:8] + "-" + hs[8:12] + "-" + hs[12:16] + "-" + hs[16:20] + "-" + hs[20:24] + "-" + hs[24:28] + "-" + hs[28:32]
    return result
}

func GenerateLoginToken ( try int ) string {
    r, err := GenerateRandomString ( 64 );
    if err != nil {
       return ""
    }
    var cnt int64
    s.db.Model(&core.LoginToken{}).Where("secret = ?", r).Count( &cnt );
    if cnt > 0 {
        if ( try == 0 ) {
            return ""
        }
        return GenerateLoginToken ( try-1 )
    }
    return r;
}

func ChangeLoginToken ( ctx echo.Context, u core.User, nextStep string, stepExtra string ) bool {
    hash, err := ReadCookie ( ctx, sessionCookie );
    if err != nil {
        return false
    }
    if hash == "" {
        return false;
    }

    var l core.LoginToken
    var count int64
    s.db.Model( &core.LoginToken{} ).Where("secret = ? AND expires_at > ? AND type = 'login'", hash, time.Now() ).Count(&count).First( &l );
    if count != 1 {
        return false
    }

    if l.UserID != u.ID {
        return false
    }

    secret := GenerateLoginToken( 10 );
    if secret == "" {
        return false
    }
    l.Secret = secret
    if nextStep == "fin" {
        l.ExpiresAt = time.Now().Add ( 24*time.Hour )
    } else {
        l.ExpiresAt = time.Now().Add ( 20*time.Minute )
    }
    l.User = u
    l.Step = nextStep
    l.StepExtra = stepExtra

    s.db.Save ( &l )
    WriteCookie ( ctx, sessionCookie, l.Secret, l.ExpiresAt )
    return true
}

func NewLoginToken ( l *core.LoginToken, ctx echo.Context, u core.User, nextStep string, stepExtra string  ) bool {
    secret := GenerateLoginToken( 10 );
    if secret == "" {
        return false
    }
    l.Secret = secret
    l.Name = "Login"
    l.Type = "login"
    if nextStep == "fin" {
        l.ExpiresAt = time.Now().Add ( 24*time.Hour )
    } else {
        l.ExpiresAt = time.Now().Add ( 20*time.Minute )
    }
    l.User = u
    l.Step = nextStep
    l.StepExtra = stepExtra

    s.db.Create ( &l )
    WriteCookie ( ctx, sessionCookie, l.Secret, l.ExpiresAt )
    return true
}

func NewAPIToken ( l *core.LoginToken, ctx echo.Context, u core.User, name string ) bool {
    secret := GenerateLoginToken( 10 );
    if secret == "" {
        return false
    }
    l.Secret = secret
    l.Name = name
    l.Type = "api"
    l.ExpiresAt = time.Now().Add ( 24*356*time.Hour )
    l.User = u

    s.db.Create ( &l )
    return true
}

func RemoveLoginToken ( ctx echo.Context ) {
    hash, err := ReadCookie ( ctx, sessionCookie );
    if err != nil {
        s.db.Where("secret = ?", hash).Delete( &core.LoginToken{} )
    }
    RemoveCookie ( ctx, sessionCookie );
}

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString returns a securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	bytes, err := GenerateRandomBytes(n)
	if err != nil {
		return "", err
	}
	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}
	return string(bytes), nil
}
